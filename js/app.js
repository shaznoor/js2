let ans1=document.getElementsByClassName('inner');
let prev=document.getElementById('prev');
let next=document.getElementById('next');

function addEvent(e){
    const modal = document.getElementById('myModal');
    let modalImg = document.getElementById('img01');
    let res=this.childNodes[1];
    let ans=res.src.split("/");
    modal.style.display = 'block';
    modalImg.src = "img/"+ans[ans.length-1];
}

for(let i=0;i<ans1.length;i++){
    ans1[i].addEventListener("click",addEvent);
}

let span = document.getElementsByClassName('close')[0];
console.log(span);
span.addEventListener("click",()=>{
    const modal = document.getElementById('myModal');
    modal.style.display = 'none';
});

function nextImage(){
    let modalImg = document.getElementById('img01');
    if(modalImg.src!=""){
        let one=modalImg.src.split("/");
        let index=one[one.length-1];
        let num=parseInt(index.slice(3).split(".")[0]);
        if(num>=6){
            num=1;
        }else{
            num++;
        }
        modalImg.src=`img/img${num}.jpg`;
    }
}

function prevImage(){
    let modalImg = document.getElementById('img01');
    if(modalImg.src!=""){
        let one=modalImg.src.split("/");
        let index=one[one.length-1];
        let num=parseInt(index.slice(3).split(".")[0]);
        if(num<=1){
            num=6;
        }else{
            num--;
        }
        modalImg.src=`img/img${num}.jpg`;
    }
}

next.addEventListener('click',nextImage);
prev.addEventListener('click',prevImage);
